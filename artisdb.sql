CREATE SCHEMA IF NOT EXISTS `artisdb` DEFAULT 

CHARACTER SET utf8 ;
USE `artisdb` ;

CREATE TABLE IF 

NOT EXISTS `artisdb`.`holdings` (
  `stockName` 

VARCHAR(50) NOT NULL,
  `holdings` INT(11) NOT NULL,
  
 `strategyId` INT(11) NOT NULL DEFAULT '0',
 
`cashBalance` FLOAT NULL DEFAULT '0',
  PRIMARY KEY 

(`stockName`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = 

utf8;


-- 

-----------------------------------------------------
-- Table `artisdb`.`stockpricerecords`
-- 

-----------------------------------------------------
CREATE TABLE IF NOT EXISTS 

`artisdb`.`stockpricerecords` (
  `id` INT(11) NOT 

NULL AUTO_INCREMENT,
  `ticker` VARCHAR(10) NOT NULL,
  

`timeInspected` TIMESTAMP NOT NULL DEFAULT 

CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  

`price` FLOAT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = 

InnoDB
DEFAULT CHARACTER SET = utf8;


-- 

-----------------------------------------------------
-- Table `artisdb`.`strategies`
-- 

-----------------------------------------------------
CREATE TABLE IF NOT EXISTS `artisdb`.`strategies` (
  

`id` INT(11) NOT NULL AUTO_INCREMENT,
  `isActive` 

BOOLEAN NULL DEFAULT '1',
  `name` VARCHAR(50) NOT 

NULL,
  `closePercentage` INT(11) NOT NULL,
  `entrySize` INT(11) 

NOT NULL DEFAULT '50',
  `ticker` VARCHAR(10) NOT 

NULL,
  `profitValue` DOUBLE NULL DEFAULT NULL,
  

PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- 

-----------------------------------------------------
-- Table `artisdb`.`trades`
-- 

-----------------------------------------------------
CREATE TABLE IF NOT EXISTS `artisdb`.`trades` (
  `id` 

INT(11) NOT NULL AUTO_INCREMENT,
  `stock` VARCHAR(4) 

NOT NULL,
  `price` FLOAT NOT NULL,
  `size` INT(11) 

NOT NULL,
  `lastStateChange` DATETIME NOT NULL,
  

`tradeType` VARCHAR(45) NOT NULL,
  `tradeState` VARCHAR(45) NOT NULL,
  `strategyId` INT(11) NOT 

NULL,
  PRIMARY KEY (`id`),
  INDEX `strategyId` 

(`strategyId` ASC),
  CONSTRAINT `trades_ibfk_1`
    

FOREIGN KEY (`strategyId`)
    REFERENCES 

`artisdb`.`strategies` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- 

-----------------------------------------------------
-- Table `artisdb`.`twomovingaveragesstrategies`
-- 

-----------------------------------------------------
CREATE TABLE IF NOT EXISTS 

`artisdb`.`twomovingaveragesstrategies` (
  `id` INT

(11) NOT NULL,
  `longTime` INT(11) NOT NULL,
  

`shortTime` INT(11) NOT NULL,
  PRIMARY KEY (`Id`),
  

CONSTRAINT `twomovingaveragesstrategies_ibfk_1`
    

FOREIGN KEY (`id`)
    REFERENCES 

`artisdb`.`strategies` (`id`))
ENGINE = InnoDB
DEFAULT 

CHARACTER SET = utf8;


-- 

-----------------------------------------------------
-- Table `artisdb`.`users`
-- 

-----------------------------------------------------
CREATE TABLE IF NOT EXISTS `artisdb`.`users` (
  

`UserId` INT(11) NOT NULL AUTO_INCREMENT,
  

`FirstName` VARCHAR(45) NULL DEFAULT NULL,
  `Surname` 

VARCHAR(45) NULL DEFAULT NULL,
  `Branch` VARCHAR(45) 

NULL DEFAULT NULL,
  `Address` VARCHAR(55) NULL 

DEFAULT NULL,
  `City` VARCHAR(45) NULL DEFAULT NULL,
  

`Phone` VARCHAR(15) NULL DEFAULT NULL,
  `Email` 

VARCHAR(25) NULL DEFAULT NULL,
  PRIMARY KEY 

(`UserId`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = 

utf8;