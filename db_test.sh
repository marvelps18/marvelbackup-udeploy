!/bin/bash
# If /root/.my.cnf exists then it won't ask for root password
if [ -f /root/.my.cnf ]; then
        echo "Creating new database..."
        mysql -h mysql.training.local -e "CREATE DATABASE IF NOT EXISTS ${dbname};"
        echo "Database successfully created!"
        echo "Showing existing databases..."
        mysql -e "show databases;"
        echo ""
        echo "Creating new user..."
        mysql -h mysql.training.local -e "CREATE USER  IF NOT EXISTS ${username}@'localhost' IDENTIFIED BY 'password';"
        echo "User successfully created!"
        echo ""
        mysql -h mysql.training.local -u root -pneueda -e "GRANT ALL PRIVILEGES ON TESTING.* TO ${username}@'localhost';"
        mysql -h mysql.training.local -u root -pneueda -e "FLUSH PRIVILEGES;"
    exit
else
        echo "Creating new database..."
        mysql -h mysql.training.local -u root -pneueda -e "CREATE DATABASE IF NOT EXISTS ${dbname};"
        echo "Database successfully created!"
        echo "Showing existing databases..."
        mysql -h mysql.training.local -u root -pneueda -e "show databases;"
        echo ""
        mysql -h mysql.training.local -u root -pneueda -e "CREATE USER IF NOT EXISTS ${username}@'localhost' IDENTIFIED BY 'password';"
        echo "User_test successfully created!"
        echo ""
        echo "Granting ALL privileges on 'TESTING' to 'user_test'!"
        mysql -h mysql.training.local -u root -pneueda -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO ${username}@'localhost';"
        mysql -h mysql.training.local -u root -pneueda -e "FLUSH PRIVILEGES;"
        echo "You're good now :)"
        mysql -h mysql.training.local -u root -pneueda -e "use  ${dbname}; SHOW TABLES" | wc -l
 exit
fi