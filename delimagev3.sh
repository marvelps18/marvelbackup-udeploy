#!/bin/bash

DOCKERREPOS="/home/docker_registry/docker/registry/v2/repositories"
DOCKERTAG="_manifests/tags"
DOCKERSHAS="index/sha256"
DOCKERREV="_manifests/revisions/sha256"
BLOBS="/home/docker_registry/docker/registry/v2/blobs/sha256"

if (( $# < 2 ))
then
        echo "USAGE: $0 <imageName> <tag> " 1>&2
        exit 1
fi

IMAGENAME=$1
TAG=$2

# Get the 2 digits for the image
cd $DOCKERREPOS/$IMAGENAME/$DOCKERTAG/$TAG/$DOCKERSHAS
sha2dig=$(ls | cut -c -2 )
shafull=$(ls)
echo "Sha 2 Dig = $sha2dig"

#Remove directory with tag # first
cd $DOCKERREPOS/$IMAGENAME/$DOCKERTAG
echo "Removing $TAG from  $(pwd))"
rm -rf $TAG

#Remove sha from revisions
cd $DOCKERREPOS/$IMAGENAME/$DOCKERREV
echo "Removing $shafull from  $(pwd)"
rm -rf $shafull

#Remove sha from blobs
cd $BLOBS/$sha2dig
echo "Removing $shafull from $(pwd)"
rm -rf $shafull

# Clean up
docker exec -it registry registry garbage-collect /etc/docker/registry/config.yml