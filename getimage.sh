#!/bin/bash

for image in $(curl -s http://docker.conygre.com:5000/v2/_catalog | tr '[,:]' '\n')
do
        #echo "$image" | sed -e 's/"//g' -e '/}$/d'
        image=$(echo "$image" | sed -e 's/"//g' -e '/}$/d' -e 's/{repositories//' | sed '/^[    ]*$/d' )
        echo "$image"
        #curl -s http://docker.conygre.com:5000/v2/$image/tags/list | sed -e 's/^.*tags"//' | tr -d ']:"[}'
        curl -s http://docker.conygre.com:5000/v2/$image/tags/list | sed -e 's/^.*tags"//' | tr -d ']:"[}' | awk '{print "\t"$0}'
done